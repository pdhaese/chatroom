# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This is a basic chat app made using node.js, socket.io, and javascript.  This allows users to make chat
rooms, view users in the room, message the room, and message users in the room.

This app uses maps I created from arrays instead of Javascript.map

pdhaese@wustl.edu