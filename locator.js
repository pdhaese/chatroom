
var allids = [];
var alltimes = [];
var alldistances = [];
var iterator = 0;
var floorplanbeacons = [96850,53506,99423,76106,18573];
var xcoors = [50,700,500,230,425];
var ycoors = [50,170,240,330,500];


function searchByUser(){
  var username = document.getElementById("searchuser").value; // Get the info from the form
  document.getElementById("theuser").innerHTML = username;
  var begdate = document.getElementById("searchdatebeg").value;
  var begtime = document.getElementById("searchtimebeg").value;
  var enddate = document.getElementById("searchdateend").value;
  var endtime = document.getElementById("searchtimeend").value;
  var beg = encodeURIComponent(begdate + ' ' + begtime);
  var end = encodeURIComponent(enddate + ' ' + endtime);
  var myurl = "http://ec2-34-210-27-148.us-west-2.compute.amazonaws.com/~pdhaese/locator/retrievepersonlocation.php?partid=" + encodeURIComponent(username) + "&timestamp1=" + beg + "&timestamp2=" + end;
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": myurl,
    //"url": "http://ec2-34-210-27-148.us-west-2.compute.amazonaws.com/~pdhaese/locator/retrievepersonlocation.php?partid=will&timestamp1=2017-11-15%2000%3A48%3A53&timestamp2=2017-11-15%2000%3A48%3A55",
    "method": "GET",
    "headers": {
      "cache-control": "no-cache",
      "postman-token": "c00090e1-573e-6c16-2e98-e3d359cb37be"
    }
  }
  $.ajax(settings).done(function (response) {
    var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
    parseUserData(jsonData.results);
  });
}

function searchByRoom(){
  var roomnumber = document.getElementById("searchroom").value; // Get the info from the form
  var begdate = document.getElementById("searchdatebegroom").value;
  var begtime = document.getElementById("searchtimebegroom").value;
  var enddate = document.getElementById("searchdateendroom").value;
  var endtime = document.getElementById("searchtimeendroom").value;
  var beg = encodeURIComponent(begdate + ' ' + begtime);
  var end = encodeURIComponent(enddate + ' ' + endtime);
  var myurl = "http://ec2-34-210-27-148.us-west-2.compute.amazonaws.com/~pdhaese/locator/roomsearch.php?beaconid=" + encodeURIComponent(roomnumber) + "&timestamp1=" + beg + "&timestamp2=" + end;
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": myurl,
    //"url": "http://ec2-34-210-27-148.us-west-2.compute.amazonaws.com/~pdhaese/locator/retrievepersonlocation.php?partid=will&timestamp1=2017-11-15%2000%3A48%3A53&timestamp2=2017-11-15%2000%3A48%3A55",
    "method": "GET",
    "headers": {
      "cache-control": "no-cache",
      "postman-token": "c00090e1-573e-6c16-2e98-e3d359cb37be"
    }
  }
  $.ajax(settings).done(function (response) {
    var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
    if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
      //parseRoom(jsonData.results);
    }
    alert(response);
    console.log(response);
  });
}

function getFloorPlans(){
  var myurl = "http://ec2-34-210-27-148.us-west-2.compute.amazonaws.com/~pdhaese/locator/floorplans.php";
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": myurl,
    //"url": "http://ec2-34-210-27-148.us-west-2.compute.amazonaws.com/~pdhaese/locator/retrievepersonlocation.php?partid=will&timestamp1=2017-11-15%2000%3A48%3A53&timestamp2=2017-11-15%2000%3A48%3A55",
    "method": "GET",
    "headers": {
      "cache-control": "no-cache",
      "postman-token": "c00090e1-573e-6c16-2e98-e3d359cb37be"
    }
  }
  $.ajax(settings).done(function (response) {
    var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
    if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
      //parseRoom(jsonData.results);
    }
    alert(response);
    console.log(response);
  });
}

function parseUserData(eventsarray){
  iterator = 0;
  allids = [];
  alltimes = [];
  alldistances = [];

  var myids = [];
  var mytimes = [];
  var mydistances = [];
  var timestamp = '';
  var distance = 3;
  for(i = 0; i < eventsarray.length; i++){
    var checktime = eventsarray[i].time;
    var checkdistance = eventsarray[i].distance;
    var checkid = eventsarray[i].id;
    if(checktime == timestamp){
      if(checkdistance < distance){
        distance = checkdistance;
        myids = [];
        mytimes = [];
        mydistances = [];
        myids.push(checkid);
        mytimes.push(checktime);
        mydistances.push(checkdistance);
      } else if (checkdistance == distance) {
        myids.push(checkid);
        mytimes.push(checktime);
        mydistances.push(checkdistance);
      }
    } else{
      if(timestamp != ''){
        allids = allids.concat(myids);
        alltimes = alltimes.concat(mytimes);
        alldistances = alldistances.concat(mydistances);
        timestamp = eventsarray[i].time;
        distance = eventsarray[i].distance;
        myids = [];
        mytimes = [];
        mydistances = [];
        myids.push(checkid);
        mytimes.push(checktime);
        mydistances.push(checkdistance);
      } else {
        timestamp = checktime;
        distance = checkdistance;
        myids.push(checkid);
        mytimes.push(checktime);
        mydistances.push(checkdistance);
      }
    }
  }
  allids = allids.concat(myids);
  alltimes = alltimes.concat(mytimes);
  alldistances = alldistances.concat(mydistances);
  displayLocations();
}

function displayLocations(){
  var displaytime = alltimes[iterator];
  var myx = 0;
  var myy = 0;
  var myid = allids[i];
  document.getElementById("draw").innerHTML = '';
  var canvas = document.getElementById("draw");
  var ctx = canvas.getContext("2d");
  var img=document.getElementById("floorplanimage");
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.drawImage(img,10,10);
  ctx.strokeStyle="#FF0000";
  document.getElementById("thedate").innerHTML = displaytime;
  for(i = 0; i <alltimes.length; i++){
    if(alltimes[i] == displaytime){
      var myradius = getRadius(alldistances[i]);
      var myx = getX(allids[i]);
      var myy = getY(allids[i]);
      ctx.beginPath();
      ctx.arc(myx,myy,myradius,0,2*Math.PI);
      ctx.stroke();
    }
  }
}

function getRadius(dist){
  if(dist == 0){
    return 20;
  }
  if(dist == 1){
    return 40;
  }
  if(dist == 2){
    return 60;
  }
  if(dist == 2){
    return ;
  }
}

function getX(myid){
  for(j = 0; j < floorplanbeacons.length; j++){
    if(floorplanbeacons[j] == myid){
      return xcoors[j];
    }
  }
}

function getY(myid){
  for(k = 0; k < floorplanbeacons.length; k++){
    if(floorplanbeacons[k] == myid){
      return ycoors[k];
    }
  }
}

function prev(){
  var checkstamp = alltimes[iterator];
  for(t=iterator; t>=0; t--){
    if(alltimes[t] != checkstamp){
      iterator = t;
      displayLocations();
      break;
    } else if(t == 0){
      alert("There are no previous data points.  Please do a new query to look at earlier information");
    }
  }
}

function next(){
  var checkstamp = alltimes[iterator];
  for(t=iterator; t<alltimes.length; t++){
    if(alltimes[t] != checkstamp){
      iterator = t;
      displayLocations();
      break;
    } else if(t == alltimes.length -1){
      alert("There are no later data points.  Please do a new query to look at later information");
    }
  }
}

function getfloornames(){
  var test = '';
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "floorplans.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
    listfloornames(jsonData.results);
	}, false); // Bind the callback to the load event
	xmlHttp.send(test); // Send the data
}

function listfloornames(eventsarray){
	document.getElementById("events").innerHTML = "";
	var eventTable = "<table class='events'> <tr class='currentmonth'><th colspan='7'>Your events for the month</th></tr>";
	var padding = "";
	eventids = [];
	for (var j = 0; j < eventsarray.length; j++){
		var par = eventsarray[j].date.toString();
		console.log(par);
		var thisdate = par.split("-");
		var str_thisdate = thisdate[1] + "/" + thisdate[2] + "/" + thisdate[0];
		padding += "<tr class='currentday'>" + eventsarray[j].title + "<br>" + str_thisdate + "<br>Time: " + eventsarray[j].time + "<br>" + "Event Id:" + eventsarray[j].id + "<br><br><br></tr>";
		eventids.push(eventsarray[j].id);
	}
	eventTable += padding;
	eventTable += "</table>";
	document.getElementById("events").innerHTML = eventTable;
}

document.getElementById("search_by_user").addEventListener("click", searchByUser, false);
document.getElementById("search_by_room").addEventListener("click", searchByRoom, false);
document.getElementById("prev").addEventListener("click", prev, false);
document.getElementById("next").addEventListener("click", next, false);



var c=document.getElementById("draw");
var ctx=c.getContext("2d");
var img=document.getElementById("floorplanimage");
ctx.drawImage(img,10,10);
