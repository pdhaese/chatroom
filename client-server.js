// Require the packages we will use:
var allusers = [];
var rooms = [];
var creators = [];
var bannedusers = [];
var bannedrooms = [];
var userrooms = [];
var usersinrooms = [];
var passwords = [];
var http = require("http"),
	socketio = require("socket.io"),
	fs = require("fs");

// Listen for HTTP connections.  This is essentially a miniature static file server that only serves our one file, client.html:
var app = http.createServer(function(req, resp){
	// This callback runs when a new connection is made to our HTTP server.

	fs.readFile("client.html", function(err, data){
		// This callback runs when the client.html file has been read from the filesystem.

		if(err) return resp.writeHead(500);
		resp.writeHead(200);
		resp.end(data);
	});
});
app.listen(3456);

// Do the Socket.IO magic:
var io = socketio.listen(app);
io.sockets.on("connection", function(socket){
	// This callback runs when a new Socket.IO connection is established.
	socket.on('make_room',function(data){
		console.log("messagesss: "+data["myuser"]+' ' +data["roomname"] );
		var taken = false;
		for (i = 0; i < rooms.length; i++) {
    	if(rooms[i] == data['roomname']){
				taken = true;
			}
		}
		if(!taken){
			console.log("not taken");
			rooms.push(data["roomname"]);
			creators.push(data['myuser']);
			passwords.push(data['mypass']);
			io.sockets.emit("create_room",{thisuser:data["myuser"],thisroomname:data["roomname"]}); // broadcast the message to other users
		 }
		 //else{
		// 	console.log("room name taken else statement");
		// 	io.sockets.emit("cannot_create_room", {takenroomname:data["roomname"]});
		// }
	});
	socket.on('delete_room', function(data){
		var rights = false;
		for (i = 0; i < rooms.length; i++) {
			if(rooms[i] == data['roomname']){
				if(creators[i] == data['myuser']){
					rights = true;
					rooms.splice(i,1);
					creators.splice(i,1);
					passwords.splice(i,1);
				}
			}
		}
		if(rights){
			for(i = 0; i < userrooms.length;i++){
				if(userrooms[i] == data['roomname']){
					userrooms.splice(i,1);
					usersinrooms.splice(i,1);
				}
			}
			io.sockets.emit("room_deleted",{allrooms:rooms,deletedroom:data['roomname']}); // broadcast the message to other users
		}
	});
	socket.on('add_to_roomlist', function(data){ //add in "users in this chat"
		var count = 0;
		var activeusers = [];
		var allowed = true;
		var curruser = data['myuser'];
		var currroom = data['myroom'];
		for (i = 0; i < bannedusers.length; i++) {
			if(bannedusers[i] == curruser){
				if(bannedrooms[i] == currroom){
					allowed = false;
				}
			}
		}
		//MAY NEED TO ADD BACK IN!!!!!!
		for(i = 0; i < userrooms.length; i++){
			if(userrooms[i] == currroom){
				if(usersinrooms[i] == curruser){
					count = count + 1;
					if(count>1){
						allowed = false;
					}
				}
			}
		}
		if(allowed){
			for(i = 0; i <userrooms.length; i++){
				if(userrooms[i] == currroom){
					activeusers.push(usersinrooms[i]);
				}
			}
			io.sockets.emit("users_in_room",{usernames:activeusers, thisuser:curruser, thisroom: currroom});
		}
	});

	socket.on("login", function(data){
		var myuser = data['myuser'];
		var allowed = true;
		for(i = 0; i <allusers.length;i++){
			if(allusers[i] == myuser){
				allowed = false;
			}
		}
		if(allowed){
			allusers.push(myuser);
			socket.emit("loggedin",{myuser:myuser,myrooms:rooms});
		} else {
			socket.emit('failed_login',{myuser:myuser});
		}
	});

	socket.on('logout',function(data){
		var myuser = data['myuser'];
		var myroom = data['myroom'];
		var activeusers = [];
		for(i = 0; i <allusers.length;i++){
			if(allusers[i] == myuser){
				allusers.splice(i,1);
			}
		}
		for(i = 0; i <allusers.length;i++){
			if(allusers[i] == myuser){
				allusers.splice(i,1);
			}
		}
		for(i = 0; i <usersinrooms;i++){
			if(usersinrooms[i] == myuser){
				usersinrooms.splice(i,1);
				userrooms.splice(i,1);
			}
		}
		for(i = 0; i <userrooms.length; i++){
			if(userrooms[i] == myroom){
				activeusers.push(usersinrooms[i]);
			}
		}
		io.sockets.emit("userlogout", {myname:myuser,myroom:myroom,activeusers:activeusers});
	});
	socket.on('private_message', function(data){
		var privateroom = data['myroom'];
		var privateuser = data['myuser'];
		var privatemessage = data['mymessage'];
		var sender = data['sender'];
		io.sockets.emit("deliver_private_message",{myuser:privateuser,mymessage:privatemessage,sender:sender,myroom:privateroom});
	});
	socket.on('ban_user',function(data){
		var banneduser = data['myuser'];
		var bannedchat = data['myroom'];
		var activeusers = [];
		for(i = 0; i < usersinrooms; i++){
			if(banneduser == usersinrooms[i]){
				usersinrooms.splice(i,1);
				userrooms.splice(i,1);
			}
		}
		bannedusers.push(banneduser);
		bannedrooms.push(bannedchat);
		for(i = 0; i <userrooms.length; i++){
			if(userrooms[i] == bannedchat){
				activeusers.push(usersinrooms[i]);
			}
		}
		io.sockets.emit("banning_user",{myuser:banneduser,usersinroom:activeusers,myroom:bannedchat});
	});

	socket.on('kick_user',function(data){
		var kickeduser = data['myuser'];
		var myroom = data['myroom'];
		var activeusers = [];
		for(i = 0; i < usersinrooms.length; i++){
			if(kickeduser == usersinrooms[i]){
				usersinrooms.splice(i,1);
				userrooms.splice(i,1);
			}
		}
		for(i = 0; i <userrooms.length; i++){
			if(userrooms[i] == myroom){
				activeusers.push(usersinrooms[i]);
			}
		}
		io.sockets.emit("kicking_user",{myuser:kickeduser,usersinroom:activeusers,myroom:myroom});
	});

	socket.on('leave_room_user',function(data){
		var userthatleft = data['myuser'];
		var myroom = data['myroom'];
		var activeusers = [];
		for(i = 0; i < usersinrooms.length; i++){
			if(userthatleft == usersinrooms[i]){
				usersinrooms.splice(i,1);
				userrooms.splice(i,1);
			}
		}
		for(i = 0; i <userrooms.length; i++){
			if(userrooms[i] == myroom){
				activeusers.push(usersinrooms[i]);
			}
		}
		io.sockets.emit("user_leaving",{myuser:userthatleft,usersinroom:activeusers,myroom:myroom});
	});

	socket.on('join_room', function(data){
		var curruser = data['myuser'];
		var currroom = data['roomname'];
		var currpass = data['mypass'];
		var allowed = true;
		for (i = 0; i < bannedusers.length; i++) {
			if(bannedusers[i] == curruser){
				if(bannedrooms[i] == currroom){
					allowed = false;
				}
			}
		}
		console.log(allowed);
		if(allowed){
			var certified = false;
			for (i = 0; i < rooms.length; i++) {
				if(rooms[i] == currroom){
					if(passwords[i] == ""){
						certified = true;
					} else {
						if(passwords[i] == currpass){
							certified = true;
						}
					}
				}
			}
			console.log(certified);
			if(certified){
				userrooms.push(currroom);
				usersinrooms.push(curruser);
				var roomowner = '';
				for( i = 0; i < rooms.length; i++){
					if(currroom == rooms[i]){
						roomowner = creators[i];
					}
				}
				socket.emit('joining_room',{newroom:currroom,owner:roomowner})
			}
			else{
				socket.emit('roompassword_failed',{thisuser:curruser})
			}
		}
	});
	socket.on('message_to_server', function(data) {
		// This callback runs when the server receives a new message from the client.
		io.sockets.emit("message_to_client",{myuser:data["myuser"],mymessage:data['mymessage'],myroom:data['myroom']}) // broadcast the message to other users
	});
});
